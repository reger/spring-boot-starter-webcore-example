package com.reger.test.webcore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.lang.reflect.Array;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.reger.core.model.ResponseEntity;
import com.reger.core.utils.MapUtils;
import com.reger.web.permission.Permission;
import com.reger.web.permission.PermissionGroup;

import io.swagger.annotations.Api;

@Api("测试控制器")
@RestController
@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class SpringBootStarterWebcoreExampleApplication {

	private static final Logger log = LoggerFactory.getLogger(SpringBootStarterWebcoreExampleApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringBootStarterWebcoreExampleApplication.class, args);
	}
	
	@GetMapping("/test/job")
	@Permission(group=Const.TEST_PERMISSION_GROUP,value="T(Reg).a('name', #name).a('key', 'value').to()",expand="select count(1) from User where name=#{name}")
	@Permission(group=Const.TEST1_PERMISSION_GROUP,value="T(Reg).asList(#name)",expand="select count(1) from User where name=?")
	public ResponseEntity<?> testJob(@RequestParam String name) {
		return ResponseEntity.success();
	}

	@Bean(Const.TEST_PERMISSION_GROUP)
	PermissionGroup permissionGroup(){
		return (map,expand)->{
			log.info("权限组{}请求到达,权限参数 {},拓展参数{}",Const.TEST_PERMISSION_GROUP,map,expand);
			return true;
		};
	}
	@Bean(Const.TEST1_PERMISSION_GROUP)
	PermissionGroup permissionGroup1(){
		return (list,expand)->{
			log.info("权限组{}请求到达,权限参数 {},拓展参数{}",Const.TEST1_PERMISSION_GROUP,list,expand);
			return true;
		};
	}
}
